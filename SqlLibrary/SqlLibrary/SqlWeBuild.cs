﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SqlLibrary
{
    public static class SqlWeBuild
    {
        /// <summary>
        /// State Variables
        /// </summary>
        private static string cnnString = "Data Source=webuild.database.windows.net;Initial Catalog=webuilddb;User ID=programmingisart;Password=programming2U;Connect Timeout=30;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        /// <summary>
        /// Static prop to get the cnnstring
        /// </summary>
        public static string CnnString
        {
            get
            {
                return cnnString;
            }
        }
    }
}
