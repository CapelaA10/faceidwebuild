﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace weBuildC.Users
{
    public class User
    {
        /// <summary>
        /// Proprieties
        /// </summary>
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Acess { get; set; }
        public bool Active { get; set; }
        public static int Count { get; set; }

        /// <summary>
        /// Constructer
        /// </summary>
        /// <param name="id">Id of the user</param>
        /// <param name="username">Username of the user</param>
        /// <param name="password">Password of the user</param>
        /// <param name="acess">Acess type of the user can be a administrator , manager or incharge</param>
        /// <param name="active">If the user is active or not</param>
        public User(int id, string username, string password, string acess, bool active)
        {
            this.Id = id;
            this.Username = username;
            this.Password = password;
            this.Acess = acess;
            this.Active = active;
            Count++;
        }

        public User()
        {
            Count++;
        }
    }
}
