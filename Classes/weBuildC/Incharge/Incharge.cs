﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace weBuildC.Incharge
{
    public class Incharge
    {
        /// <summary>
        /// Proprieties
        /// </summary>
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public bool Active { get; set; }
        public static int Count { get; set; }

        /// <summary>
        /// Constructer
        /// </summary>
        /// <param name="id">Id of the incharge</param>
        /// <param name="name">Name of incharge</param>
        /// <param name="lastName">LastName of the incharge</param>
        /// <param name="active">If the uincharge is active or not</param>
        public Incharge(int id, string name, string lastName, bool active)
        {
            this.Id = id;
            this.Name = name;
            this.LastName = lastName;
            this.Active = active;
            Count++;
        }
    }
}
