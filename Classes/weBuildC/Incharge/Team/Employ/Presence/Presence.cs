﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace weBuildC.Incharge.Team.Employ.Presence
{
    public class Presence
    {
        /// <summary>
        /// Proprieties
        /// </summary>
        public int Id { get; set; }
        public DateTime WorkEntryTime { get; set; }
        public DateTime WorkOutTime { get; set; }
        public DateTime LunchEntryTime { get; set; }
        public DateTime LunchOutTime { get; set; }
        public static int Count { get; set; }

        /// <summary>
        /// Constructer
        /// </summary>
        /// <param name="id">Id of the presence</param>
        /// <param name="workEntryTime">Time of entry the work</param>
        /// <param name="workOutTime">Time of out the work</param>
        /// <param name="lunchEntryTime">Time of entry the lunch</param>
        /// <param name="lunchOutTime">Time of out the lunch</param>
        public Presence(int id, DateTime workEntryTime, DateTime workOutTime, DateTime lunchEntryTime, DateTime lunchOutTime)
        {
            this.Id = id;
            this.WorkEntryTime = workEntryTime;
            this.WorkOutTime = workOutTime;
            this.LunchEntryTime = lunchEntryTime;
            this.LunchOutTime = LunchOutTime;
            Count++;
        }
    }
}
