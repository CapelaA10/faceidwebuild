﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace weBuildC.Incharge.Team.Employ.LoanEmploy
{
    public class LoanForEmploy
    {
        /// <summary>
        /// Proprieties
        /// </summary>
        public int Id { get; set; }
        public float Amount { get; set; }
        public DateTime RegisterDate { get; set; }
        public static int Count { get; set; }

        /// <summary>
        /// Constructer
        /// </summary>
        /// <param name="id">Id of the loan give to the employ</param>
        /// <param name="amount">Amount of the loan</param>
        /// <param name="registerDate">Date of the loan</param>
        public LoanForEmploy(int id, float amount, DateTime registerDate)
        {
            this.Id = id;
            this.Amount = amount;
            this.RegisterDate = registerDate;
            Count++;
        }
    }
}
