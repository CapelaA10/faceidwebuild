﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace weBuildC.Incharge.Team.Employ
{
    public class Employ
    {
        /// <summary>
        /// Proprieties
        /// </summary>
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public Byte[] Image { get; set; }
        public string Nactionality { get; set; }
        public DateTime DtaNas { get; set; }
        public static int Count { get; set; }

        /// <summary>
        /// Constructer
        /// </summary>
        /// <param name="id">Employ Id</param>
        /// <param name="name">Employ name</param>
        /// <param name="lastName">Employ Last Name</param>
        /// <param name="image">Employ image</param>
        /// <param name="nacionality">Employ nacionality</param>
        /// <param name="dtaNas">Employ Birthday date</param>
        public Employ(int id, string name, string lastName, Byte[] image, string nacionality, DateTime dtaNas)
        {
            this.Id = id;
            this.Name = name;
            this.LastName = lastName;
            this.Image = image;
            this.Nactionality = nacionality;
            this.DtaNas = dtaNas;
            Count++;
        }

        /// <summary>
        /// Employ constructer with just 2 arguments
        /// </summary>
        /// <param name="id">Id of the employ</param>
        /// <param name="name">Name of the employ</param>
        public Employ(int id, string name)
        {
            this.Id = id;
            this.Name = name;
            Count++;
        }

        /// <summary>
        /// Constructer
        /// </summary>
        /// <param name="id">Employ Id</param>
        /// <param name="name">Employ name</param>
        /// <param name="lastName">Employ Last Name</param>
        /// <param name="nacionality">Employ nacionality</param>
        /// <param name="dtaNas">Employ Birthday date</param>
        public Employ(int id, string name, string lastName, string nacionality, DateTime dtaNas)
        {
            this.Id = id;
            this.Name = name;
            this.LastName = lastName;
            this.Nactionality = nacionality;
            this.DtaNas = dtaNas;
            Count++;
        }

        /// <summary>
        /// Employ constructer
        /// </summary>
        /// <param name="name">name </param>
        /// <param name="lastName">last name </param>
        public Employ(string name, string lastName, string nacionality)
        {
            this.Name = name;
            this.LastName = lastName;
            this.Nactionality = nacionality;
            Count++;
        }
    }
}
