﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace weBuildC.Incharge.Team.Loan
{
    public class Loan
    {
        /// <summary>
        /// Propreties
        /// </summary>
        public int Id { get; set; }
        public float Amount { get; set; }
        public DateTime DateOfStart { get; set; }
        public DateTime DateOfEnd { get; set; }
        public static int Count { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id">Loan Id</param>
        /// <param name="amount">Loan amount</param>
        /// <param name="dateOfStart">Loan date of start</param>
        /// <param name="dateOfEnd">Loan date of end</param>
        public Loan(int id, float amount, DateTime dateOfStart, DateTime dateOfEnd)
        {
            this.Id = id;
            this.Amount = amount;
            this.DateOfStart = dateOfStart;
            this.DateOfEnd = dateOfEnd;
            Count++;
        }

        public Loan()
        {
            Count++;
        }
    }
}
