﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace weBuildC.Incharge.Team
{
    public class Team
    {
        /// <summary>
        /// Propreties
        /// </summary>
        public int Id { get; set; }
        public string  Country { get; set; }
        public static int Count { get; set; }

        /// <summary>
        /// Constructer
        /// </summary>
        /// <param name="id">Team Id</param>
        /// <param name="country"> Team Country</param>
        public Team(int id, string country)
        {
            this.Id = id;
            this.Country = country;
            Count++;
        }
    }
}
