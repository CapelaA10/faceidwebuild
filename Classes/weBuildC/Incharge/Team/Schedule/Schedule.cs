﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace weBuildC.Incharge.Team.Schedule
{
    public class Schedule
    {
        /// <summary>
        /// Proprieties
        /// </summary>
        public int Id { get; set; }
        public int WeekDays { get; set; }
        public TimeSpan StartHour { get; set; }
        public TimeSpan EndHour { get; set; }
        public TimeSpan StartLunchHour { get; set; }
        public TimeSpan EndLunchHour { get; set; }
        public static int Count { get; set; }

        /// <summary>
        /// Constructer
        /// </summary>
        /// <param name="id">Id of the schedule</param>
        /// <param name="weekDays">Number of the days in the week 1-7</param>
        /// <param name="startHour">Starting hour of the day</param>
        /// <param name="endHour">End hour of the day</param>
        /// <param name="startLunchHour">Start hour of the lunch</param>
        /// <param name="endLunchHour">End time of the lunch</param>
        public Schedule(int id, int weekDays, TimeSpan startHour, TimeSpan endHour, TimeSpan startLunchHour, TimeSpan endLunchHour)
        {
            this.Id = id;
            this.WeekDays = weekDays;
            this.StartHour = startHour;
            this.EndHour = endHour;
            this.StartLunchHour = startLunchHour;
            this.EndLunchHour = endLunchHour;
            Count++;
        }
    }
}
