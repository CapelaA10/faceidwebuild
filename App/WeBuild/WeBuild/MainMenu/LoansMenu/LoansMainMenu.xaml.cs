﻿using SqlLibrary;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeBuild.MainMenu.LoansMenu.SubMenu;
using weBuildC.Incharge.Team.Loan;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WeBuild.MainMenu.LoansMenu
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoansMainMenu : ContentPage
	{
        //Local Variables
        int userId;

        //Instance loan object
        Loan loan = new Loan();

		public LoansMainMenu(int userId)
		{
			InitializeComponent();

            //Equals the variable that is inserted and that variable is passed to the local variables userId
            this.userId = userId;

        }

        private void InsertMenuBtt_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new InsertALoan(userId));
        }

        //Void to load the amount of the loan that is avaliable 
        public void LoadLoans()
        {
            //Connection string
            string connectionString = SqlWeBuild.CnnString;
            
            //Variable
            DateTime today = DateTime.Today;

            //Try catch with the erro
            try
            {

                //Using sql connection
                using (SqlConnection cnn = new SqlConnection(connectionString))
                {
                    //String with the sql command
                    string query = "SELECT LOAN.ID ,L_AMOUNT, L_DTAIN, L_DTAOU FROM LOAN INNER JOIN TEAM ON LOAN.L_TEAM = TEAM.ID INNER JOIN INCHARGE ON INCHARGE.ID = TEAM.T_INCHAR INNER JOIN USERS ON INCHARGE.I_USER = USERS.ID WHERE USERS.ID = @ID AND L_DTAIN <= @TODAY AND L_DTAOU >= @TODAY ";

                    //Creating a sql command
                    SqlCommand cmm = new SqlCommand(query, cnn);

                    //Opening connection
                    cnn.Open();

                    //Parameters
                    cmm.Parameters.AddWithValue("@ID", userId);
                    cmm.Parameters.AddWithValue("@TODAY", today);

                    //using data reader
                    using (SqlDataReader reader = cmm.ExecuteReader())
                    {
                        //While to reader
                        while (reader.Read())
                        {
                            //Loan amount + what is in the db
                            loan.Amount = reader.GetFloat(1);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                DisplayAlert("Erro", ex.Message, "Try Again!");
            }

            //Display the user the loan amount
            LabelAmount.Text = loan.Amount.ToString();
        }

        private void LoadLoansPossibleBtt_Clicked(object sender, EventArgs e)
        {
            //Entrer the void to know the loan that he can apply
            LoadLoans();
        }
    }
}