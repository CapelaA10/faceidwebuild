﻿using SqlLibrary;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using weBuildC.Incharge.Team.Employ;
using weBuildC.Users;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WeBuild.MainMenu.LoansMenu.SubMenu
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InsertALoan : ContentPage
    {
        //Local variables
        int userId;

        //List employ for the list view
        List<Employ> employs = new List<Employ>();

        public InsertALoan(int userId)
        {
            InitializeComponent();

            //Passing the values to the local variables
            this.userId = userId;

            //Cicle to load the employs
            LoadEmploys();
        }

        private void BttInsertLoanForEmploy_Clicked(object sender, EventArgs e)
        {
            //Connection string
            string connectionString = SqlWeBuild.CnnString;

            //Try catch with the erro
            try
            {

                //Instancing sql connection object 
                using (SqlConnection cnn = new SqlConnection(connectionString))
                {
                    //String with the sql command
                    string query = "INSERT INTO LOAN_FOR_EMPLOY (ID, L_F_AMOUNT, L_F_REGISTERDATE, L_F_EMPLOY) VALUES ((SELECT MAX(ID) FROM LOAN_FOR_EMPLOY)+1, @AMOUNT, @DATE, @EMPLOYID)";

                    //Creating a sql command
                    SqlCommand cmm = new SqlCommand(query, cnn);

                    //Opening connection
                    cnn.Open();

                    //Parameters to insert in the db
                    cmm.Parameters.AddWithValue("@EMPLOYID", EmployId.Text);
                    cmm.Parameters.AddWithValue("@AMOUNT", EmployLoanAmount.Text);
                    cmm.Parameters.AddWithValue("@DATE", DateTime.Now);

                    //Executing
                    cmm.ExecuteNonQuery();

                    //Display alert to the user
                    DisplayAlert("Loan", "Inserted you can now give the money", "Thank you!");
                }
            }
            catch(Exception ex)
            {
                DisplayAlert("Erro", ex.Message, "Try again!");
            }
        }

        private void LoadEmploys()
        {
            //Connection string
            string connectionString = SqlWeBuild.CnnString;

            //Try catch with the erro
            try
            {

                //Instancing sql connection
                using (SqlConnection cnn = new SqlConnection(connectionString))
                {
                    //String with the sql command
                    string query = "SELECT EMPLOYS.ID, EMPLOYS.E_NAME FROM EMPLOYS  INNER JOIN TEAM ON TEAM.ID = EMPLOYS.E_TEAM INNER JOIN INCHARGE ON INCHARGE.ID = TEAM.T_INCHAR WHERE INCHARGE.I_USER = @ID";

                    //Creating a sql command
                    SqlCommand cmm = new SqlCommand(query, cnn);

                    //Opening connection
                    cnn.Open();

                    //New parameter id
                    cmm.Parameters.AddWithValue("@ID", userId);

                    //Using sql reader with the execute reader
                    using (SqlDataReader reader = cmm.ExecuteReader())
                    {
                        //While the reader has info to read
                        while (reader.Read())
                        {
                            employs.Add(new Employ(reader.GetInt32(0), reader.GetString(1)));
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                DisplayAlert("Erro", ex.Message, "Try again!");
            }

            //Items source equals the employ list
            ListViewEmploys.ItemsSource = employs;
        }
    }
}