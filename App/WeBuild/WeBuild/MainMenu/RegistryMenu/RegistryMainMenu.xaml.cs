﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeBuild.MainMenu.RegistryMenu.SubMenus;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WeBuild.MainMenu.RegistryMenu
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RegistryMainMenu : ContentPage
    { 
        //Local variables
        int userId;

        public RegistryMainMenu (int userId)
		{
			InitializeComponent ();

            //Equals the variable that is inserted and that variable is passed to the local variables userId
            this.userId = userId;
        }

        private void WorkOutBtt_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new WorkOutP());
        }

        private void LunchInBtt_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new LunchInP());
        }

        private void WorkInBtt_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new WorkInP());
        }

        private void LunchOutBtt_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new LunchOutP());
        }

        private void AbsenceBtt_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new Absence());
        }
    }
}