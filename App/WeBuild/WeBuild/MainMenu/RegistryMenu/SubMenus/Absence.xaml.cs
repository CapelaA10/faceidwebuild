﻿using SqlLibrary;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WeBuild.MainMenu.RegistryMenu.SubMenus
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Absence : ContentPage
	{
		public Absence ()
		{
			InitializeComponent ();
		}

        private void InsertAbsence_Clicked(object sender, EventArgs e)
        {
            //Connection string
            string connectionString = SqlWeBuild.CnnString;

            //Try catch to the erros 
            try
            {
                //Creating a sql connection
                using (SqlConnection cnn = new SqlConnection(connectionString))
                {
                    //Query
                    string query = "INSERT INTO ABSENCE (ID, Ab_Date, Ab_Justify, Ab_IdEmploy) VALUES ((select count(ID) from ABSENCE)+1, @DATE, 0, @ID_Employ)";

                    //Sql command
                    SqlCommand cmm = new SqlCommand(query, cnn);

                    //Opening connection
                    cnn.Open();

                    //Parameters add
                    cmm.Parameters.AddWithValue("@ID_Employ", EntryIdEmploy.Text);
                    cmm.Parameters.AddWithValue("@DATE", DateTime.Now);

                    // Execute the query
                    cmm.ExecuteNonQuery();

                    //Make an display alert
                    DisplayAlert("Absence Inserted!", "", "close");

                    // Close the connection
                    cnn.Close();

                    //Back to the previous menu
                    base.OnBackButtonPressed();
                }
            }
            catch (Exception ex1)
            {
                DisplayAlert("Absence Error!", ex1.Message, "close");
            }
        }
    }
}