﻿using Android.Graphics;
using Plugin.Media;
using Plugin.Media.Abstractions;
using SqlLibrary;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WeBuild.RegisterC;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WeBuild.MainMenu.RegistryMenu.SubMenus
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class WorkInP : ContentPage
	{
        //Stream of the employ image from the taked foto 
        Stream employImage;

		public WorkInP ()
		{
			InitializeComponent();
		}

        public async void TakeFotoBtt_Clicked(object sender, EventArgs e)
        {
            //Check if the camara is avaliable
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await DisplayAlert("Camara", "The camara is not avalable", "Thank You");
                return;
            }

            //Stored information
            var stored = new StoreCameraMediaOptions()
            {
                SaveToAlbum = true,
                Name = "EmployFt.jpg",
                PhotoSize = PhotoSize.Small
            };

            //Passing the foto the variable
            var photo = await CrossMedia.Current.TakePhotoAsync(stored);

            //Check if the photo is null
            if (photo == null)
                return;

            //Employ image stream 
            employImage = photo.GetStream();

            //Passing the photo to the image scource
            FotoFaceId.Source = ImageSource.FromStream(() =>
            {
                var stream = photo.GetStream();
                photo.Dispose();
                return stream;
            });
        }

        public async void RecognitionBtt_Clicked(object sender, EventArgs e)
        {
            //Variables 
            Stream employDb = new MemoryStream();
            int verify = 2;

            //Connection string
            string connectionString = SqlWeBuild.CnnString;

            //Try catch to erros
            try
            {
                //Creating a sql connection
                using (SqlConnection cnn = new SqlConnection(connectionString))
                {
                    //Query
                    string query = "SELECT E_IMAGE FROM EMPLOYS WHERE ID=@ID";

                    //Sql command
                    SqlCommand cmm = new SqlCommand(query, cnn);

                    //Opening connection
                    cnn.Open();

                    //Parameters add
                    cmm.Parameters.AddWithValue("@ID", EntryIdEmploy.Text);

                    //Sql datareader as command execute
                    using (SqlDataReader reader = cmm.ExecuteReader())
                    {
                        //If the reader have something to read
                        if (reader.Read())
                        {
                            //Getting the photo by from the db
                            byte[] photo = (byte[])reader[0];

                            //Getting the stream of the employ image from the db 
                            employDb = new MemoryStream(photo);
                        }
                    }
                }
            }
            catch (Exception ex1)
            {
                await DisplayAlert("Register", ex1.Message, "Thank you");
            }

            //Try catch to erros
            try
            {
                //Getting the value of the verify
                verify = await FaceId.FaceId.VerifyFaceAsync(employImage, employDb);
            }
            catch (Exception ex)
            {
                await DisplayAlert("Register", ex.Message, "Thank you");
            }
           

            //Checking verify
            if (verify == 3)
            {
                //Register work in 
                RegisterCL.RegisterWorkIn(Convert.ToInt32(EntryIdEmploy.Text));

                //Altering the user
                await DisplayAlert("Register", "Completed", "Thank you");
            }
            else if (verify == 2)
                await DisplayAlert("Register", "Not Completed", "Thank you");
            else 
                await DisplayAlert("Register", "Error", "Thank you");
        }
    }
}