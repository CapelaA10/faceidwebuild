﻿using SqlLibrary;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using weBuildC.Incharge.Team.Employ;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WeBuild.MainMenu.WorkersIn
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class WorkInWorkers : ContentPage
	{
        //User id for the query
        int userId;

		public WorkInWorkers (int userId)
		{
			InitializeComponent ();

            //Equals the variable that is inserted and that variable is passed to the local variables userId
            this.userId = userId;
		}

        private void LoadWorkersInWorkBtt_Clicked(object sender, EventArgs e)
        {
            //List of employs
            List<Employ> employs = new List<Employ>();
            
            //Connection string to the db
            string connectionString = SqlWeBuild.CnnString;

            //Try catch to the error
            try
            {
                //Sql connection
                using (SqlConnection cnn = new SqlConnection(connectionString))
                {
                    //Query
                    string query = "SELECT E_NAME , E_LASTANAME , E_NACIONALITY FROM EMPLOYS INNER JOIN PRESENCE ON PRESENCE.P_EMPLOY = EMPLOYS.ID INNER JOIN TEAM ON EMPLOYS.E_TEAM = TEAM.ID INNER JOIN INCHARGE ON INCHARGE.ID = TEAM.T_INCHAR INNER JOIN USERS ON INCHARGE.I_USER = USERS.ID WHERE USERS.ID = @ID AND PRESENCE.P_REGISTERENTRY IS NOT NULL AND PRESENCE.P_REGISTEROUT IS NULL";

                    //Sql command
                    using (SqlCommand cmm = new SqlCommand(query, cnn))
                    {
                        //Opening connection
                        cnn.Open();

                        //Parameters
                        cmm.Parameters.AddWithValue("@ID", userId);

                        //Sqldatareader
                        using (SqlDataReader reader = cmm.ExecuteReader())
                        {
                            //Reader have something to read
                            while (reader.Read())
                            {
                                //Adding to the list the employs
                                employs.Add(new Employ(reader.GetString(0), reader.GetString(1), reader.GetString(2)));                            
                            }

                        }
                    }
                }
            }
            catch(Exception ex)
            {
                DisplayAlert("Erro", ex.Message, "Thank you");
            }

            //List view adding source
            ListViewWorkersIn.ItemsSource = employs;
        }
    }
}