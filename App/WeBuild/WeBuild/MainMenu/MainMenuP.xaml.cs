﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WeBuild.MainMenu.LoansMenu;
using WeBuild.MainMenu.RegistryMenu;
using WeBuild.MainMenu.WorkersIn;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WeBuild.MainMenu
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainMenuP : ContentPage
    {
        //Local variables
        int userId;

        //Main menu page constructer tthat takes the userId to know who is the incharge
        public MainMenuP(int userId)
        {
            InitializeComponent();

            //Equals the variable that is inserted and that variable is passed to the local variables userId
            this.userId = userId;
        }

        private void LoansMenuBtt_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new LoansMainMenu(userId));
        }

        private void RegistryMenuBtt_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new RegistryMainMenu(userId));
        }

        private void WorkersInWorkBtt_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new WorkInWorkers(userId));
        }
    }
}