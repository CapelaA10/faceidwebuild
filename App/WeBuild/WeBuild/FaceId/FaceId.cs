﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Cognitive.Face;
using Xamarin.Forms;

namespace WeBuild.FaceId
{
    public static class FaceId
    {
        public static async Task<int> VerifyFaceAsync(Stream employ, Stream employDb)
        {  
            //Face detected or not if 2 is false, 1 is a erro, 3 true 
            int faceIdPer = 2;

            //Getting the value of the result
            faceIdPer = await GetVeri(employ, employDb);

            //Returning the value 
            return faceIdPer;
        }

        public static async Task<int> GetVeri(Stream employ, Stream employDb)
        {
            //Azure key
            var azureFaceAPIKey = "1eaa32a394d94c3f9f5394d61da6ab5a"; // Your API Key

            //End point and subscription key
            FaceClient.Shared.Endpoint = Endpoints.WestEurope;
            FaceClient.Shared.SubscriptionKey = azureFaceAPIKey;

            //Indentifying faces
            var face1 = await FaceClient.Shared.DetectFacesInPhoto(employ);
            var face2 = await FaceClient.Shared.DetectFacesInPhoto(employDb);

            //Awaiting the face1 e face2 array construction 
            Thread.Sleep(3000);

            //Getting the result 
            var result = await FaceClient.Shared.Verify(face1[0].Id, face2[0].Id);

            //Checking the confidence or a error 
            if (result.Confidence >= 0.55)
                return 3;
            else if (result.Confidence < 0.55)
                return 2;
            else
                return 1;
        }
    }
}
