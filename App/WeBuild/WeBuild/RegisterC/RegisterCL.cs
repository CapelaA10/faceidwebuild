﻿using SqlLibrary;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace WeBuild.RegisterC
{
    public static class RegisterCL
    {
        //Variables 
        private static string connectionString = SqlWeBuild.CnnString;

        /// <summary>
        /// Register work in
        /// </summary>
        /// <param name="id">id employ</param>
        public static void RegisterWorkIn(int id)
        {
            //Variables
            DateTime todayNow = DateTime.Now;

            //SqlConnection
            using (SqlConnection cnn = new SqlConnection(connectionString))
            {
                //Command
                string query = "INSERT INTO PRESENCE (ID, P_REGISTERENTRY, P_EMPLOY) VALUES ((SELECT MAX(ID) FROM PRESENCE) + 1, @DATE, @ID)";

                //Object Command
                using (SqlCommand cmm = new SqlCommand(query, cnn))
                {
                    //Opening connection
                    cnn.Open();

                    //Parameters
                    cmm.Parameters.AddWithValue("@ID", id);
                    cmm.Parameters.AddWithValue("@DATE", todayNow);

                    //ExecuteNonQuery
                    cmm.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Register Lunch in
        /// </summary>
        /// <param name="id">id employ</param>
        public static void RegisterLunchIN(int id)
        {
            //Variables
            DateTime todayNow =  DateTime.Now;
            DateTime today = DateTime.Today;

            //Using sqlconnection
            using (SqlConnection cnn = new SqlConnection(connectionString))
            {
                //Query
                string query = "UPDATE PRESENCE SET P_REGISTERENTRYLUNCH=@DATE WHERE P_EMPLOY = @ID AND CONVERT(DATE, P_REGISTERENTRY) = @DATETODAY";

                //Sql command object
                using (SqlCommand cmm = new SqlCommand(query, cnn))
                {
                    //Opening connection
                    cnn.Open();

                    //Parameters
                    cmm.Parameters.AddWithValue("@ID", id);
                    cmm.Parameters.AddWithValue("@DATE", todayNow);
                    cmm.Parameters.AddWithValue("@DATETODAY", today);

                    //Execute the command
                    cmm.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Register Lunch out
        /// </summary>
        /// <param name="id">id employ</param>
        public static void RegisterLunchOut(int id)
        {
            //Variables
            DateTime todayNow = DateTime.Now;
            DateTime today = DateTime.Today;

            //Object sql connection
            using (SqlConnection cnn = new SqlConnection(connectionString))
            {
                //Query
                string query = "UPDATE PRESENCE SET P_REGISTEROUTLUNCH=@DATE WHERE P_EMPLOY = @ID AND CONVERT(DATE, P_REGISTERENTRY) = @DATETODAY";

                //SqlCommand object
                using (SqlCommand cmm = new SqlCommand(query, cnn))
                {
                    //Connection open
                    cnn.Open();

                    //Parameters
                    cmm.Parameters.AddWithValue("@ID", id);
                    cmm.Parameters.AddWithValue("@DATE", todayNow);
                    cmm.Parameters.AddWithValue("@DATETODAY", today);

                    //Execute command
                    cmm.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Register Work out
        /// </summary>
        /// <param name="id">id employ</param>
        public static void RegisterWorkOut(int id)
        {
            //Variables
            DateTime todayNow = DateTime.Now;
            DateTime today = DateTime.Today;

            //Sql connection
            using (SqlConnection cnn = new SqlConnection(connectionString))
            {
                //Query
                string query = "UPDATE PRESENCE SET P_REGISTEROUT=@DATE WHERE P_EMPLOY = @ID AND CONVERT(DATE, P_REGISTERENTRY) = @DATETODAY";

                //Sql command
                using (SqlCommand cmm = new SqlCommand(query, cnn))
                {
                    //Connection open
                    cnn.Open();

                    //Parameters
                    cmm.Parameters.AddWithValue("@ID", id);
                    cmm.Parameters.AddWithValue("@DATE", todayNow);
                    cmm.Parameters.AddWithValue("@DATETODAY", today);

                    //Execute non query, command
                    cmm.ExecuteNonQuery();
                }
            }
        }
    }
}
