﻿using SqlLibrary;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeBuild.MainMenu;
using weBuildC.Users;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WeBuild
{
    [XamlCompilation (XamlCompilationOptions.Compile)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        //Btt login clicked event 
        private void LoginBtt_Clicked(object sender, EventArgs e)
        {
            //Local variables
            string password = PasswordEntry.Text;
            string username = UsernameEntry.Text;
            User user = null;

            //Connection string
            string connectionString = SqlWeBuild.CnnString;

            //Try catch with the erro 
            try
            {

                //Using sql connection
                using (SqlConnection cnn = new SqlConnection(connectionString))
                {
                    //String with the sql command
                    string query = "SELECT ID, U_USERNAME, U_PASSWORD, U_ACESS, U_ACTIVE FROM USERS WHERE U_USERNAME = @USERNAME AND U_PASSWORD = @PASSWORD";

                    //Creating a sql command
                    SqlCommand cmm = new SqlCommand(query, cnn);

                    //Opening connection
                    cnn.Open();

                    //Parameters to the sql query
                    cmm.Parameters.AddWithValue("@USERNAME", username);
                    cmm.Parameters.AddWithValue("@PASSWORD", password);

                    //Using sql reader with the execute reader
                    using (SqlDataReader reader = cmm.ExecuteReader())
                    {
                        //While the reader has info to read
                        if (reader.Read())
                        {
                            //Check if the user is not deleted
                            if (reader.GetBoolean(4) == true)
                            {
                                user = new User(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetBoolean(4));
                            }
                        }
                    }

                    //Type of login and if the password is wrong or username 
                    if (user.Acess == "I")
                    {
                        DisplayAlert("Login", "InCharge Completed", "Thank you!");
                        Navigation.PushModalAsync(new MainMenuP(user.Id));
                    }
                    else if (user.Acess == "A")
                    {
                        DisplayAlert("Login", "Admin login are not exepected", "Thank you!");
                    }
                    else if (user.Acess == "M")
                    {
                        DisplayAlert("Login", "Manager login are not exepected", "Thank you!");
                    }
                    else
                    {
                        DisplayAlert("Login", "Wrong Username/Password", "Try again");
                    }
                }
            }
            catch(Exception ex)
            {
                DisplayAlert("Error", ex.Message , "try again!");
            }
        }
    }
}
